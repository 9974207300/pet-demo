import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Input, Select, Button } from 'antd';
import 'antd/dist/antd.css';

const { Option } = Select;


class AddContact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: 'pending',
            petname: ''
        }
    }


    handleSubmit = (e) => {
        const { petname, status } = this.state;
        const id = new Date().getUTCMilliseconds().toString() + new Date().getTime().toString();
        if (petname !== '') {
            fetch(`https://petstore.swagger.io/v2/pet`, {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: id,
                    category: {
                        id: id,
                        name: petname
                    },
                    name: petname,
                    photoUrls: [
                        ''
                    ],
                    tags: [{
                        id: id,
                        name: ''
                    }],
                    status: status
                })
            }).then(response => response.json())
                .then(
                    data => {
                        console.log('data', data)
                        this.setState({
                            petsarray: data
                        })
                    }
                )
                .catch(error => this.setState({ error }));
        }
    };

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        })
    }

    render() {
        return (
            <div className="post-container" >
                <h1 className="post_heading" >Add Pets</h1>
                <div className="form" >
                    <div>
                        <label>Pet Name:</label>
                        <Input className="w-100"
                            type="text"
                            name='petname'
                            value={this.state.petname}
                            onChange={(e) => this.handleChange(e)}
                            placeholder="Enter pet Name" />
                    </div>
                    <div className='selecttag'>
                        <label className='petstatus'>Pet Status:</label>
                        <Select size="large"
                            name='status'
                            defaultValue={this.state.status}
                            onChange={(e) => this.handleChange(e)} >
                            <Option className="account-modal-role-option"
                                value='pending' > Pending </Option>
                            <Option className="account-modal-role-option"
                                value='available' > Available </Option>
                            <Option className="account-modal-role-option"
                                value='sold' > Sold </Option>
                        </Select>
                    </div>
                    <Button type="primary" size='large'
                        onClick={(e) => this.handleSubmit(e)}>
                        ADD PETS
                    </Button>
                </div>
            </div>
        );
    }


};


export default AddContact;