import React, { Component } from 'react';
import { connect } from 'react-redux';
import Contact from './Contact';
import { Table, Select } from 'antd';
import 'antd/dist/antd.css';

const { Option } = Select;


class AllContact extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: 'pending',
            petsarray: null
        }
    }

    componentWillMount() {
        this.getpets(this.state.status);
    }

    getpets = (status) => {
        // const { status } = this.state
        console.log('state', status)
        fetch(`https://petstore.swagger.io/v2/pet/findByStatus?status=${status}`, {
            method: "GET",
        }).then(response => response.json())
            .then(
                data => {
                    console.log('data', data)
                    this.setState({
                        petsarray: data
                    })
                }
            )
            .catch(error => this.setState({ error }));
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value,
        })
        this.getpets(e.target.value);
    }

    render() {
        const dataSource = this.state.petsarray;

        const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name',
            },
            {
                title: 'Index',
                dataIndex: 'id',
                key: 'id',
            },
        ];
        return (
            <div>
                <div className='statusvalue'>
                    <Select size="large"
                        name='status'
                        defaultValue={this.state.status}
                        onChange={(e) => this.handleChange(e)}>
                        <Option value='pending'>Pending</Option>
                        <Option value='available'>Available</Option>
                        <Option value='sold'>Sold</Option>
                    </Select>
                </div>
                <div className='clearfix' />
                {this.state.petsarray ?
                    <h1 className="post_heading">Pets List</h1>
                    : <h1 className="post_heading">Empty Pets List</h1>
                }
                {this.state.petsarray &&
                    <Table dataSource={dataSource} columns={columns}  />
                }
            </div>
        );
    }
}
;


const mapStateToProps = (state) => {
    return {
        contacts: state
    };
};





export default connect(mapStateToProps)(AllContact);
